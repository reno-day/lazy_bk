package com.linln.config.listener;

import com.linln.modules.system.domain.Uv;
import com.linln.modules.system.service.UvService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Date;

import java.util.Map;

public class OnlineListener
        implements HttpSessionListener
{

    @Autowired
    private UvService uvService;




    // 当用户与服务器之间session断开时触发该方法
    @Override
    public void sessionDestroyed(HttpSessionEvent se)
    {
        try {
            HttpSession session = se.getSession();
            ServletContext application = session.getServletContext();
            String sessionId = session.getId();
            Map<String , String> uvMap = (Map<String , String>)
                    application.getAttribute("uvMap");
            Long id = Long.valueOf(uvMap.get(sessionId));
            if (id!= null) {
                Uv beUv = uvService.getById(id);
                beUv.setUpdateDate(new Date());
                //EntityBeanUtil.copyProperties(beUv, uv);
                uvService.save(beUv);
            }
        }catch (Exception e){}


    }
}
