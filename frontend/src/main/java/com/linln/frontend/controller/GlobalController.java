package com.linln.frontend.controller;



import com.linln.component.shiro.ShiroUtil;
import com.linln.modules.system.domain.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;


@ControllerAdvice(basePackages = "com.linln.frontend.controller")
public class GlobalController {

    @ModelAttribute
    public void addSettings(Model model) {
        // 增加登录信息
        User user = ShiroUtil.getSubject();
        model.addAttribute("userInfo",user);
    }
}
